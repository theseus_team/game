using UnityEngine;
using System.Collections;

public class lightFlicker : MonoBehaviour {

	GameObject light;
	public float minimumSecondsOn = .2f;
	public float minimumSecondsOff = .1f;
	public float chanceToOnPerTenthSecondOff = .3f;
	public float chanceToOffPerTenthSecondOn = .1f;
	float waitTimeBeforeNextCheck = 0f;
	
	// Use this for initialization
	void Start () 
	{
		light = gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(waitTimeBeforeNextCheck > 0f)
		{
			waitTimeBeforeNextCheck -= Time.deltaTime;
		}
		else
		{
			light.light.enabled = flickerCheck();
		}
	}
	
	bool flickerCheck()
	{
		float roll = Random.Range (0.0f, 1.0f);
		if(light.light.enabled)
		{
			if(roll <= chanceToOffPerTenthSecondOn)  //light will flicker off
			{
				waitTimeBeforeNextCheck += minimumSecondsOff;
				return false;
			}
			else
			{
				waitTimeBeforeNextCheck += .1f;
				return true;
			}
		}
		else
		{
			if(roll <= chanceToOnPerTenthSecondOff)  //light will flicker on
			{
				waitTimeBeforeNextCheck += minimumSecondsOn;
				return true;
			}
			else
			{
				waitTimeBeforeNextCheck += .1f;
				return false;
			}
		}
		return true;
	}
}
