using UnityEngine;
using System.Collections;

public class smokePreStart : MonoBehaviour {
	
	public ParticleEmitter p;
	public float initialTime = 10.0f;
	// Use this for initialization
	void Start () {
		for(int i=0; i<initialTime; i++)
		{
			p.Simulate(1.0f);
		}
	}
}
