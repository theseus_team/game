using UnityEngine;
using System.Collections;

public class stoplightTimer : MonoBehaviour {
	
	GameObject light;
	public float greenTime = 15.0f;
	public float redTime = 15.0f;
	public float yellowTime = 2.0f;
	float waitTimeBeforeNextChange = 7.0f;
	int state = 0; //0=red, 1=green, 2=yellow
	// Use this for initialization
	void Start () {
		light = gameObject;
		light.light.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(waitTimeBeforeNextChange > 0f)
		{
			waitTimeBeforeNextChange -= Time.deltaTime;
		}
		else
		{
			changeLight();
		}
	}
	
	void changeLight()
	{
		if(state == 0)
		{
			light.light.color = Color.green;
			state = 1;
			waitTimeBeforeNextChange = greenTime;
			return;
		}
		if(state == 1)
		{
			light.light.color = Color.yellow;
			state = 2;
			waitTimeBeforeNextChange = yellowTime;
			return;
		}
		if(state == 2)
		{
			light.light.color = Color.red;
			state = 0;
			waitTimeBeforeNextChange = redTime;
			return;
		}
	}
}
