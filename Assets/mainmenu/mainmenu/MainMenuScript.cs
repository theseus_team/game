using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {
	
	public GUISkin MainMenuSkin;
	
	void mainMenu()
	{
		Rect outer = new Rect(Screen.width - 250, Screen.height-250, 250, 200);
		Rect newG = new Rect(55, 100, 180, 40);
		Rect exitG = new Rect(55, 150, 180, 40);
		
		GUIStyle style = MainMenuSkin.FindStyle("customStyle");
		/*testStyle.fontSize = 32;
		testStyle.normal.textColor = Color.white;
		testStyle.hover.textColor = Color.cyan;
		testStyle.active.textColor = Color.red;*/
		
		
		GUI.BeginGroup (outer);
		if(GUI.Button (newG, "New Game", style))
		{
			MainMenuScript script = (MainMenuScript)GetComponent("MainMenuScript");
			script.enabled = false;
		}
		if(GUI.Button (exitG, "Exit Game", style))
		{
			MainMenuScript script = (MainMenuScript)GetComponent("MainMenuScript");
			script.enabled = false;
			QuitMenuScript quitMenu = (QuitMenuScript)GetComponent("QuitMenuScript");
			quitMenu.enabled = true;
		}
		GUI.EndGroup();
	}
	
	void OnGUI () 
	{
		GUI.skin = MainMenuSkin;
		mainMenu ();
	}
}
