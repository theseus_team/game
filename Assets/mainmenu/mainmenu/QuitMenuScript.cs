using UnityEngine;
using System.Collections;

public class QuitMenuScript : MonoBehaviour {

	public GUISkin QuitMenuSkin;
	
	void quitMenu()
	{
		Rect outer = new Rect(Screen.width - 450, Screen.height-250, 450, 200);
		Rect labelRect = new Rect(55, 100, 180, 40);
		Rect yesRect = new Rect(55, 150, 90, 40);
		Rect noRect = new Rect(155, 150, 90, 40);
		
		GUIStyle style = QuitMenuSkin.FindStyle("customStyle");
		/*testStyle.fontSize = 32;
		testStyle.normal.textColor = Color.white;
		testStyle.hover.textColor = Color.cyan;
		testStyle.active.textColor = Color.red;*/
		
		
		GUI.BeginGroup (outer);
		GUI.Label (labelRect, "Are you sure you wanna quit?", style);
		if(GUI.Button (yesRect, "Yes", style))
		{
			Application.Quit ();
		}
		if(GUI.Button (noRect, "No", style))
		{
			QuitMenuScript quitMenu = (QuitMenuScript)GetComponent("QuitMenuScript");
			quitMenu.enabled = false;
			MainMenuScript mainMenu = (MainMenuScript)GetComponent("MainMenuScript");
			mainMenu.enabled = true;
		}
		GUI.EndGroup();
	}
	
	void OnGUI () 
	{
		GUI.skin = QuitMenuSkin;
		quitMenu ();
	}
}
