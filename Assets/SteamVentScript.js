#pragma strict
@script RequireComponent(ParticleSystem)
@script RequireComponent(Collider)
class SteamVentScript extends InteractiveObj{
	var on : boolean = true;
	function Start () {
	
	}
	
	function Update () {
		if (activated){
			if (on){
				disableVent();
			}
			else {
				enableVent();
			}
			activated = false;
			on = !on;
		}
	}
	function disableVent(){
		particleSystem.enableEmission = false;
		collider.enabled = false;
	}
	function enableVent(){
		particleSystem.enableEmission = true;
	}
	function OnTriggerEnter(other : Collider){
		if (other.collider.name.Contains("Player")){
			var p : PlayerScript = other.collider.GetComponent("PlayerScript");
			p.takeDamage(12);
		}
	}
}