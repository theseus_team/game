#pragma strict
class PlayerScript extends MonoBehaviour{
	var maxLife: float;
	var life: float;
	var healthUnit : Texture;
	var healthBorder : Texture;
	var inventory : ArrayList = new ArrayList();
	var inventoryMeshes : ArrayList = new ArrayList();
	var selectedItem : int;
	
	function Start(){
		maxLife = life = 50;
		selectedItem = 0;
	}
	
	function Update(){
	
		if (Input.GetKeyDown("e")){
			selectedItem++;
			if (selectedItem >= inventory.Count){
				selectedItem = 0;
			}
			var i : int = 0;
			for (var item : ItemScript in inventory){
				if (i != selectedItem){
					item.collider.enabled = false;
					item.ChangeObjectLayer(item.transform, LayerMask.NameToLayer("Unequipped"));
				}
				else{
					item.collider.enabled = true;
					item.ChangeObjectLayer(item.transform, LayerMask.NameToLayer("Equipped"));
				}
				i++;
			}
			for (var obj : GameObject in inventoryMeshes){
				obj.transform.RotateAround(Vector3(30, 20, 70), Vector3.up, 360/inventoryMeshes.Count);
			}
		}
		
		// Restarts level
		if (Input.GetKeyDown("r")){
			Application.LoadLevel("Game");
		}
	}
	
	function takeDamage(damage : float){
		life -= damage;
		if (life < 1){
			// Return to start screen
			Application.LoadLevel("Game");
		}
	}
	
	function OnGUI () {
		var healthWidth: int = 192;
		var healthHeight: int = 32;
		
		var healthMarginLeft: int = 0;
		var healthMarginTop: int = 32;
		
		var frameWidth : int = 192;
		var frameHeight: int = 64;
		
		var frameMarginLeft : int = -32;
		var frameMarginTop: int = 16;
		
		// Draw individual life units
		for (var i = 0; i < 20*(life/maxLife); i++){
			GUI.DrawTexture( Rect(frameMarginLeft+i*10, frameMarginTop, frameMarginLeft + frameWidth, 
			frameMarginTop + frameHeight), healthUnit, ScaleMode.ScaleToFit, true, 0);
		}
		
		// Draw a frame around the lifebar
		//GUI.DrawTexture( Rect(frameMarginLeft, frameMarginTop<<1, frameMarginLeft + (frameWidth<<1), 
		//    frameMarginTop + frameHeight), healthBorder, ScaleMode.StretchToFill, true, 0);
	}
	
	function OnCollisionStay(collision : Collision){
		// Pick up items and put them in your inventory
	}
	
	function OnControllerColliderHit(hit: ControllerColliderHit){
		if (hit.collider.name.Contains("PressurePlate")){
			var col : InteractiveObj = hit.collider.GetComponent("InteractiveObj");
			col.activated = true;
		}
		else if (hit.collider.tag.Contains("Weapon")){
			print("hit");
			var w : ItemScript = hit.collider.GetComponent("ItemScript");
			w.pickUp = true;
		}
		else if (hit.collider.tag.Contains("Enemy")) {
			print("hit");
			var e : BaseEnemy = hit.collider.GetComponent("BaseEnemy");
			e.dealDamage();
		}
		else {
		
		}
	}
	function Awake(){
		DontDestroyOnLoad(this);
	}
}