#pragma strict
class SpikeTrap extends BaseEnemy{
	var maxHeight : float = 0;
	function Start () {
		damageVal = 10;
	}
	
	function dist(x1:float, x2:float, y1:float, y2:float){
		return Mathf.Sqrt(Mathf.Pow(y1-y2, 2) + Mathf.Pow(x1-x2, 2));
	}
	
	function Update () {
		var p = GameObject.FindGameObjectWithTag("Player").transform.position;
		if (dist(p.x, this.transform.position.x, p.z, this.transform.position.z) < 8.1 && 
		 this.transform.position.y < maxHeight){
			this.transform.position.y += .5;
		}
	}
}