#pragma strict

var oldPos : Vector3;
var bounceY : float;
function Start () {
	oldPos = GameObject.FindGameObjectWithTag("Player").transform.position;
	bounceY = 0;
}


function Update () {
	//transform.Rotate(0, 1, 0);
	//GameObject.FindGameObjectWithTag("Player").transform.Translate(0, -1, -1);
	transform.position = GameObject.FindGameObjectWithTag("Player").transform.position;
	if (Input.GetKey("w") || Input.GetKey("s") || Input.GetKey("a") || Input.GetKey("d")){
		bounceY += .2;
	}
	transform.position.y += Mathf.Sin(bounceY) * .25;
}