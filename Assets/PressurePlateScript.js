#pragma strict
class PressurePlateScript extends InteractiveObj{
	var dy : float = -.01;
	var nearbyObjectsActivated : boolean = false;
	function Start () {
	
	}
	
	function Update () {
		if (activated){
			// depress when activated and trigger nearby objects
			if (transform.position.y > -.5){
				transform.position.y += dy;
			}
			if (!nearbyObjectsActivated){
				for (var obj : InteractiveObj in components){
					var pos = transform.position;
					var objPos = obj.transform.position;
					// Only activate components in neighboring tiles
					//print (wallSize);
					if (dist(pos.x, objPos.x, pos.z, objPos.z) <= wallSize*1.5){
						obj.activated = true;
					}
				}
				nearbyObjectsActivated = true;
			}
		}
	}
}