#pragma strict
public class BaseEnemy extends MonoBehaviour{
	public var hitByPlayer : boolean = false;
	// How long an enemy takes to recharge from a hit
	var hitTimer : int = 0;
	var maxHitTimer : int = 25;
	
	var maxDamageTimer : int = 100;
	var damageTimer : int = maxDamageTimer;
	var damageVal : float = 10;
	
	var life : float = 5;
	
	function Start () {
	}
	function Update(){
		damageTimer++;
		if (hitTimer > 0){
			hitTimer--;
		}
		if (hitTimer < 1 && hitByPlayer){
			// take damage
			life--;
			// Automatically "unhit" the enemy so that it takes another attack to retrigger it
			hitByPlayer = false;
			hitTimer = maxHitTimer;
			
			if (life < 1){
				//print("dead");
				Destroy(this.gameObject);
			}
		}
	}
	function hit(){
		this.hitByPlayer = true;
	}
	function dealDamage(){
		var p : PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent("PlayerScript");
		if (damageTimer >= maxDamageTimer){
			p.takeDamage(damageVal);
			damageTimer = 0;
		}
	}
}
