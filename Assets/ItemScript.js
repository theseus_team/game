#pragma strict

class ItemScript extends MonoBehaviour{
	var itemCenterZ : float = 76; // Point that objects rotate around
	var carried : boolean = false;
	var dontDestroy : boolean = false;
	var pickUp : boolean = false;
	function Start () {
		
	}
	
	function Update () {
		if (pickUp && !carried){
			PickUpItem();
		}
	}
	function OnCollisionStay(collision : Collision){
	
	}
	function PickUpItem(){
		// If an object collides with a player and isn't in his inventory, add it
		//if (collision.gameObject.tag.Contains("Weapon") && !carried){
			dontDestroy = true;
			var player : PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent("PlayerScript");
			
			// Unequip it by default, add to inventory, and disable collider
			ChangeObjectLayer(this.transform, LayerMask.NameToLayer("Unequipped"));
			player.inventory.Add(this);
			carried = true;
			collider.enabled = false;
			
			// Clones the object to display in the inventory
			var clonedObj : GameObject = Instantiate(this.gameObject, Vector3(30, 20, itemCenterZ), Quaternion.identity);
			Destroy(clonedObj.GetComponent(Weapon));
			Destroy(clonedObj.GetComponent(Rigidbody));
			ChangeObjectLayer(clonedObj.transform, LayerMask.NameToLayer("Inventory"));
			player.inventoryMeshes.Add(clonedObj);
			print(player.inventoryMeshes.Count);
			
			// Sets the rotation within the inventory
			var p : PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent("PlayerScript");
			//var i : int = 0;
			for (var i = 0; i < p.inventoryMeshes.Count; i++){
				var obj : GameObject = p.inventoryMeshes[i];
				obj.transform.position = Vector3(30, 20, itemCenterZ);
				
				var ang : float = 180 - 360.0*(p.selectedItem - i)/p.inventoryMeshes.Count;
				print (ang);
				obj.transform.RotateAround(Vector3(30, 20, 70), Vector3.up, ang);
			}
		//}
	}
	// Recursively change layer for all components of an obj
	function ChangeObjectLayer(trans : Transform, name : int){
		gameObject.layer = name;
	    for (var child : Transform in trans){
	        child.gameObject.layer = name;
	        ChangeObjectLayer(child, name);
	    }
	}
	function Awake(){
		if (carried || dontDestroy){
			DontDestroyOnLoad(this);
		}
	}
}