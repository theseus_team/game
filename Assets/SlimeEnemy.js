#pragma strict
//@script RequireComponent(Animation)

// Enemy that naively goes straight towards the player, disregarding walls
class SlimeEnemy extends BaseEnemy{
	//life = 10;
	var dx : float = 0;
	var dy : float = 0;
	var dz : float = 0;
	var moveTimer : float = 0;
	var maxMoveTimer : float = 80;	
	
	function Start () {
		damageVal = 4;
	}
	
	function Update () {
		super.Update();
		moveTimer++;
		var movement = transform.GetComponent(CharacterController);
		if (moveTimer > maxMoveTimer){
			moveTimer = 0;
			var ppos : Vector3 = GameObject.FindGameObjectWithTag("Player").transform.position;
			var ang = Mathf.Atan2(-transform.position.z + ppos.z, -transform.position.x + ppos.x);
			dx = Mathf.Cos(ang)*.7;
			dz = Mathf.Sin(ang)*.7;
		}
		movement.Move(Vector3(dx, 0, dz));
		dx *= .96;
		dz *= .96;
	}
	
}