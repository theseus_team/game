#pragma strict
public class MinotaurScript extends BaseEnemy{
	var startY : float;
	function Start () {
		startY = this.transform.position.y;
		damageVal = 6;
		life = 40;
		var animationSpeed : float = 12;
		for (var state : AnimationState in animation) {
	        state.speed = animationSpeed;
	    }
	}
	function Update(){
		super.Update();
		if (this.startY < this.transform.position.y)
			this.transform.position.y = startY;
		var movement = transform.GetComponent(CharacterController);
		var player = GameObject.FindGameObjectWithTag("Player");
		var ppos = player.transform.position;
		var ang = Mathf.Atan2(-transform.position.z + ppos.z, -transform.position.x + ppos.x);
		
		// Move towards player in a very rough manner
		movement.Move(Vector3(Mathf.Cos(ang)*.22, 0, Mathf.Sin(ang)*.22));
		
		// Always face the opposite direction of the camera
		// This makes the minotaur face in the general direction of the player at all times
		transform.forward = -GameObject.FindGameObjectWithTag("MainCamera").transform.forward;
	}

}