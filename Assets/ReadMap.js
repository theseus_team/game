#pragma strict
import System.IO;
public class ReadMap extends MonoBehaviour{
	static var currMap : int = 0;
	static function parse () {
		var wallSize : int = 30;
		var interactiveObjs = new ArrayList();
		var rustWallTex = Resources.Load("walltexture", Texture2D);
		var concreteWallTex = Resources.Load("concretetex", Texture2D);
		var data3 = [
		"WWWWWWWWWWWWWWWWW",
		"WDWgJgLg W WeemEW",
		"WpWggggggWOWteetW",
		"WtWgLWLgg OWeeeeW",
		"WsWgEDgggWOWteetW",
		"WtWWGWWWWWWWWGWWW",
		"WY ta  WiiiJilWOW",
		"WWWWGWWWiiiiiiWOW",
		"WdtddddWtiitiiWOW",
		"WddddddDiRiiiiWOW",
		"Wdtd dtWitiiiiWOW",
		"WWWWSWWWWWWDWWWOW",
		"WOWtatWO tffftWOW",
		"WO bbbWOWJffffWOW",
		"WOWbbbWJWfftffWOW",
		"WOWtbt OWtffftWOW",
		"WWWWWWWWWWWWWWWWW"];
		
		var data1 = [
		"WWWWWWWWWWWWWWWWWWWWWW",
		"WDDDDp t tlt t t t t W",
		"WWWWWWWWWWGWWWWWW WWWW",
		"Wt tWtRt t t tWt  t  W",
		"W   D         W   Y  W",
		"W  aWW R tWR WWt  t  W",
		"WWWGWWWWSWWWWWWWWWWWWW",
		"Wt    tWa t tSW      W",
		"Wt Ja tst x tSW      W",
		"WWWWGWWWWWWDWWWWWWWWWW",
		"WtJ JtW Wt  tW  t   tW",
		"WJtJstW Wt   t Rt t tW",
		"WWWWWDW WWWWWWDWWWWWWW",
		"W   Wt Wt R s tWt   tW",
		"W   Ws D t   Jt  l  tW",
		"WWWWWWWWWWWWWWWWWGWWWW",
		"W        Wt t tW  t tW",
		"W        Wt m t   t tW",
		"W        WWWWWWWWWWWWW"];
		
		var data2 = [
		"WWWWWWWWWWWWWWWWW",
		"WDWtJgLg W WeemEW",
		"Wp RggRggWOWteetW",
		"Wt RLWLgg OWeeeeW",
		"WsRg DgggWOWteetW",
		"Wt WGWWWWWWWWGWWW",
		"WY ta  WiiiJilWOW",
		"WttWGWWWiiiiiiWOW",
		"WdtddddWtiitiiWOW",
		"WddddddDiRiiiiWOW",
		"Wdtd dtWitiiiiWOW",
		"WWWWSWWWWWWDWWWOW",
		"WOWtatWO tffftWOW",
		"WO bbbWOWJffffWOW",
		"WOWbbbWJWfftffWOW",
		"WOWtbt OWtffftWOW",
		"WWWWWWWWWWWWWWWWW"];
		
		var maps = [data1, data2];
		var data = maps[currMap];
		
		// Goes through, character by character, and loads the map
		// W and W represent walls
		// There's one problem
		var s : Transform;
		var ms: InteractiveObj;
		for (var i = 0; i < data.length; i++){
			for (var j = 0; j < data[i].length; j++){
				if (data[i][j] == 'W' || data[i][j] == "W"){
					var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
					cube.transform.localScale = Vector3(wallSize, wallSize*1.5, wallSize);
					cube.transform.Translate(wallSize*i, wallSize>>1, wallSize * j);
					if (i > 0){
						if (data[i][j] == data[i-1][j]){
							cube.transform.localRotation.eulerAngles = Vector3(0, 90, 0);
						}
					}
					//cube.renderer.material.mainTexture = rustWallTex;
					cube.renderer.material.mainTexture = concreteWallTex;
				}
				if (data[i][j] == 's'){	
					s = Instantiate(Resources.Load("spiketrap", Transform), 
					 Vector3(wallSize*i, -5.5, wallSize * j), Quaternion.identity);
				}
				else if (data[i][j] == 'm'){
					s = Instantiate(Resources.Load("Minotaur", Transform), 
					 Vector3(wallSize*i, 11, wallSize * j), Quaternion.identity);
				}
				else if (data[i][j] == 'R'){
					s = Instantiate(Resources.Load("DireRat", Transform), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
				}
				else if (data[i][j] == 'p'){
					if (!GameObject.FindGameObjectWithTag("Player")){
						s = Instantiate(Resources.Load("Player", Transform), 
						 Vector3(wallSize*i, 7, wallSize * j), Quaternion.identity);
					}
					else {
						GameObject.FindGameObjectWithTag("Player").transform.position = Vector3(wallSize*i, 7, wallSize * j);
					}
				}
				else if (data[i][j] == 'J'){
					s = Instantiate(Resources.Load("Slime", Transform), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
				}
				else if (data[i][j] == 'Y'){
					s = Instantiate(Resources.Load("ShortSword", Transform), 
					 Vector3(wallSize*i, 5, wallSize * j), Quaternion.identity);
					s.transform.localRotation.eulerAngles.x = 45;
				}
				else if (data[i][j] == 'x'){
					s = Instantiate(Resources.Load("Axe", Transform), 
					 Vector3(wallSize*i, 5, wallSize * j), Quaternion.identity);
					s.transform.localRotation.eulerAngles.x = 45;
				}
				else if (data[i][j] == 'S'){
					ms = Instantiate(Resources.Load("SteamVent", InteractiveObj), 
					 Vector3(wallSize*i, 7, wallSize * j + (wallSize>>1)), Quaternion.identity);
					ms.transform.localRotation.eulerAngles.x = -90;
					interactiveObjs.Add(ms);
				}
				else if (data[i][j] == 't'){
					s = Instantiate(Resources.Load("Torch", Transform), 
					 Vector3(wallSize*i, 7, wallSize * j), Quaternion.identity);
					// Attach to a neighboring wall
					if (data[i][j+1] == 'W'){
						s.transform.position.z += wallSize>>1;
						s.transform.localRotation.eulerAngles.y = 90;						
					}
					else if (data[i][j-1] == 'W'){
						s.transform.position.z -= wallSize>>1;
						s.transform.localRotation.eulerAngles.y = -90;
					}
					if (data[i-1][j] == 'W'){
						s.transform.position.x -= wallSize>>1;
						s.transform.localRotation.eulerAngles.y = 0;
					}
					else if (data[i+1][j] == 'W'){
						s.transform.position.x += wallSize>>1;
						s.transform.localRotation.eulerAngles.y = 180;
					}
				}
				else if (data[i][j] == 'L'){
					s = Instantiate(Resources.Load("WallLight", Transform), 
					 Vector3(wallSize*i, 7, wallSize * j), Quaternion.identity);
				}
				else if (data[i][j] == 'D'){
					s = Instantiate(Resources.Load("DrainPipe", Transform), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
					 if (data[i][j+1] == "W" || data[i][j+1] == "W"){
					 	s.transform.localRotation.eulerAngles.y = 90;
					 }
				}
				else if (data[i][j] == 'E'){
					s = Instantiate(Resources.Load("Ladder", Transform), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
				}
				else if (data[i][j] == 'G'){
					ms = Instantiate(Resources.Load("Gate", InteractiveObj), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
					 ms.transform.localRotation.eulerAngles.z = 180;
					 ms.transform.position.y = wallSize>>1;
					 if (data[i][j+1] == "W" || data[i][j+1] == "W"){
					 	ms.transform.localRotation.eulerAngles.y = 90;
					 }
					 // The gate can be triggered by the lever, so add it to the list
					 interactiveObjs.Add(ms);
				}
				else if (data[i][j] == 'l'){
					ms = Instantiate(Resources.Load("Lever", InteractiveObj), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
					 ms.transform.localRotation.eulerAngles.z = -90;
					 if (data[i][j+1] == "W" || data[i][j+1] == "W"){
					 	ms.transform.localRotation.eulerAngles.y = 90;
					 }
					 // Give the Lever a list of all objects it can interact with
					 ms.components = interactiveObjs;
					 ms.gameObject.tag = "Trigger";
					 ms.wallSize = wallSize;		 
				}
				else if (data[i][j] == 'a'){
					ms = Instantiate(Resources.Load("PressurePlate", InteractiveObj), 
					 Vector3(wallSize*i, 0, wallSize * j), Quaternion.identity);
					 // Give the PressurePlate a list of all objects it can interact with
					 ms.components = interactiveObjs;
					 ms.gameObject.tag = "Trigger";
					 ms.wallSize = wallSize;
				}
				
				// Fancy rounded ceilings for hallways
				if (j > 0 && j < data[i].Length-1 && data[i][j+1] == 'W' && data[i][j-1] == 'W'
				&& data[i][j] != 'W'){
					s = Instantiate(Resources.Load("CurvedCeiling", Transform), 
					 Vector3(wallSize*i, wallSize>>1, wallSize * j), Quaternion.identity);
					 s.transform.localRotation.eulerAngles.x = 90;
					 s.transform.localRotation.eulerAngles.y = 90;
				}
				else if (i > 0 && i < data.Length-1 && data[i+1][j] == 'W' && data[i-1][j] == 'W'
				&& data[i][j] != 'W'){
					s = Instantiate(Resources.Load("CurvedCeiling", Transform), 
					 Vector3(wallSize*i, wallSize>>1, wallSize * j), Quaternion.identity);
					 s.transform.localRotation.eulerAngles.x = 90;
				}
			}
		}
	}
	
	// Check whether a tile is a wall tile
	static function isWall(tile){
		if (tile == 'W' || tile == 'X'){
			return true;
		}
		return false;
	}
	
	function Update () {

	}
}