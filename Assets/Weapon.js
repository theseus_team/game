#pragma strict
// Import AudioSource
@script RequireComponent(AudioSource)
@script RequireComponent(Animation)


// TO DO:
// Test having hits cause weapon recoil
public class Weapon extends ItemScript {
	
	var swingAngle :float = 0;
	
	var swingAccel : float;
	var swingVelocity : float;
	
	// State of weapon moving and player ordering another attack
	var swinging : boolean = false;
	var releaseSwing : boolean = true;
	
	// Whether the weapon is in a state of being capable of dealing damage
	var damaging : boolean = false;
	var damageValue : float = 2;
	
	var stabPos : float = 0;
	var stabVel : float = 1;
	
	//var carried : boolean = false;
	
	// Type of weapon determines type of attack
	var SWING : int = 0;
	var STAB : int = 1;
	var kinds = ["swing", "stab"];
	var kind = SWING;
	
	var swingClip : AudioClip;
	
	function Start () {
		swingAccel = 16;
		swingVelocity = swingAccel;
		// Automatically add to player inventory if carried
		if (carried){
			var player : PlayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent("PlayerScript");
			player.inventory.Add(this);
		
			// Clones the object to display in the inventory
			var clonedObj : GameObject = Instantiate(this.gameObject, Vector3(30, 20, itemCenterZ), Quaternion.identity);
			Destroy(clonedObj.GetComponent(Weapon));
			Destroy(clonedObj.GetComponent(Rigidbody));
			ChangeObjectLayer(clonedObj.transform, LayerMask.NameToLayer("Inventory"));
			player.inventoryMeshes.Add(clonedObj);
		}
	}
	
	// Simple swinging function
	function Swing(){
		transform.rotation.eulerAngles.x += swingAngle;
		transform.rotation.eulerAngles.z += swingAngle/10;
		swingAngle += swingVelocity;
		if (swingAngle >= 130){
			swingVelocity = -swingAccel * .8;
			damaging = false;
			collider.enabled = false;
		}
		else if (swingAngle <= -10 && swingVelocity < 0){
			swingVelocity = swingAccel;
			// If the player releases the attack button, stop swinging
			if (releaseSwing){
				swinging = false;
			}
			else {
				audio.PlayOneShot(swingClip, 1);
				damaging = true;
				collider.enabled = true;
			}
		}
	}
	
	// Jabs weapon straight forward
	function Stab(){
		// Get camera.forward and move in that direction, then reverse to pull back
		var forward = Camera.main.transform.forward;
		stabPos += stabVel;
		
		transform.position.x += forward.x * stabPos;
		transform.position.y += forward.y * stabPos;
		transform.position.z += forward.z * stabPos;
		
		if (stabPos >= 12){
			// Note: we use an imported "audio" obj to play sounds
			audio.PlayOneShot(swingClip, 1);
			audio.pitch = .4;
			stabVel = -.75;
			damaging = true;
			collider.enabled = true;
		}
		if (stabPos <= 0){
			stabVel = 2.2;
			damaging = false;
			if (releaseSwing){
				swinging = false;
			}
			collider.enabled = false;
		}
	}
	
	// Here we'll handle where a weapon should be displayed in relation to the player
	function Update () {
		super.Update();
		if (carried){
			var cam = GameObject.FindGameObjectWithTag("MainCamera");
			var camPos = cam.transform.position;
			
			// Control other cameras
			var backupCams = GameObject.FindGameObjectsWithTag("Cam");
			for (backupCam in backupCams){
				if (backupCam.name == "EquippedCam"){
					backupCam.transform.position = camPos;
					backupCam.transform.rotation = cam.transform.rotation;
				}
				else if (backupCam.name == "InventoryCam"){
					//backupCam.transform.position = camPos;
					//backupCam.transform.rotation = cam.transform.rotation;
				}
			}
			
			// Displayed position in front of camera
			var swordDest = camPos + (cam.transform.forward * 5);
		
			// un-translate so we can move it in front of the player
			transform.Translate(0, 0, 0);
			// un-rotate so it'll be in line with the player
			transform.Rotate(0, 0, 0);
			
			// We should rotate the sword around the player a couple degrees
			// so it appears on the side of the screen
			
			
			// Using Rotate() didn't work for some odd reason, so we do it manually
			// Rotation is based on weapon type
			if (kind == SWING){
				transform.rotation.eulerAngles.x = 0 + cam.transform.rotation.eulerAngles.x;
				transform.rotation.eulerAngles.y = 10 + cam.transform.rotation.eulerAngles.y;
				transform.rotation.eulerAngles.z = 20 + cam.transform.rotation.eulerAngles.z;
			}
			else if (kind == STAB){
				transform.rotation.eulerAngles.x = 90 + cam.transform.rotation.eulerAngles.x;
				transform.rotation.eulerAngles.y = -8 + cam.transform.rotation.eulerAngles.y;
				transform.rotation.eulerAngles.z = 10 + cam.transform.rotation.eulerAngles.z;
			}
			
			//Using Translate() just doesn't work, so I had to manually change each value
			transform.position.x = swordDest.x;
			transform.position.y = swordDest.y - 2.8;
			transform.position.z = swordDest.z;
			
			// Display the weapon to the player's right side
			transform.RotateAround(cam.transform.position, Vector3(0, 1, 0), 15);
			
			// Perform an attack and continue until the button's released
			if (Input.GetMouseButtonDown(0)){
				swinging = true;
				releaseSwing = false;
				damaging = true;
			}
			if (Input.GetMouseButtonUp(0)){
				releaseSwing = true;
			}
			if (swinging){
				if (kind == STAB){
					Stab();
				}
				else if (kind == SWING){
					Swing();
				}
			}
		}
	}
	
	function OnCollisionStay(collision : Collision){
		super.OnCollisionStay(collision);
		if (damaging){
			// Do damage to enemies
			if (collision.collider.gameObject.tag == "Enemy"){
				var enemy : BaseEnemy = collision.collider.gameObject.GetComponent(BaseEnemy);
				enemy.hit();
			}
			// Trigger events when specific objects are whacked
			else if (collision.collider.gameObject.tag == "Trigger"){
				var o : InteractiveObj = collision.collider.gameObject.GetComponent(InteractiveObj);
				o.activated = true;
				if (o.gameObject.name.Contains("Lever")){
					var l : LeverScript = o.gameObject.GetComponent(LeverScript);
					l.finishedTurning = false;
				}
			}
		}
	}	
	function OnCollisionExit(collision : Collision){
		/*
		if (collision.collider.gameObject.tag == "Enemy"){
			//var enemy : BaseEnemy = collision.collider.gameObject.GetComponent(BaseEnemy);
			//enemy.hitByPlayer = true;
		}
		if (collision.collider.gameObject.tag == "Trigger"){
			print("Connected");
		}
		*/
	}

}
