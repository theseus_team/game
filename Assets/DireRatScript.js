#pragma strict
class DireRatScript extends BaseEnemy{
	var animationSpeed : float;
	function Start () {
		animationSpeed = 12;
		for (var state : AnimationState in animation) {
	        state.speed = animationSpeed;
	    }
	}
	
	function Update () {
		super.Update();
		
		var movement = transform.GetComponent(CharacterController);
		var player = GameObject.FindGameObjectWithTag("Player");
		var ppos = player.transform.position;
		var ang = Mathf.Atan2(-transform.position.z + ppos.z, -transform.position.x + ppos.x);
		// Move towards player in a very rough manner
		movement.Move(Vector3(Mathf.Cos(ang)*.22, 0, Mathf.Sin(ang)*.22));
		
		// Always face towards the camera
		transform.forward.x = -Mathf.Cos(ang);
		transform.forward.z = -Mathf.Sin(ang);
	}
}