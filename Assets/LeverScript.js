#pragma strict
class LeverScript extends InteractiveObj{	
	var finishedTurning : boolean = false;
	var nearbyObjectsActivated : boolean = false;
	function Start () {
		// Check neighboring squares on the map
		// Anything directly above, below, or to the sides will be linked to this lever
		// Whenever this lever is hit, set that object's "active" state to true
		// This'll mostly be used for opening gates
		//print(components[0].activate);
	}
	
	function Update () {
		// Once this lever is triggered, rotate it away from its old direction
		// After rotating ~90 directions, activate nearby components
		if (activated){
			if (!finishedTurning){
				if (nearbyObjectsActivated == false){
					transform.rotation.eulerAngles.z += 2;
					if (transform.rotation.eulerAngles.z >= 356){
						finishedTurning = true;
						nearbyObjectsActivated = true;
					}
				}
				else if (nearbyObjectsActivated == true){
					transform.rotation.eulerAngles.z -= 2;
					if (transform.rotation.eulerAngles.z <= 266){
						finishedTurning = true;
						nearbyObjectsActivated = false;
					}
				}
			}
			else{
				for (var obj : InteractiveObj in components){
					var pos = transform.position;
					var objPos = obj.transform.position;
					// Only activate components in neighboring tiles
					//print (wallSize);
					if (dist(pos.x, objPos.x, pos.z, objPos.z) <= wallSize){
						obj.activated = true;
					}
				}
				activated = false;
			}
		}
		
	}
}